(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.offsiteForm = {

    attach: function (context) {

      once('mp-processed', '#commerce-mercado-pago-form', context).forEach(
        function () {

          var data = drupalSettings.commerceMercadoPago;

          var preference_id = data.preference_id;
          var public_key = data.public_key;

          const mp = new MercadoPago(public_key);

          window.addEventListener(
            "message",
            (event) => {
              if (event.origin !== "mercadopago.com") {
                return;
              }
            },
            FALSE,
          );
          // autoreturn on iframe?
          // window.parent.location.href = data.return;

        }

      );

    }

  };

}(jQuery, Drupal, drupalSettings));
