<?php

namespace Drupal\commerce_mercado_pago\PluginForm\Checkout;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_price\Price;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use MercadoPago\Item;
use MercadoPago\Payer;
use MercadoPago\Preference;
use MercadoPago\Shipments;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Off-site form for Mercado Pago Checkout.
 *
 * This is provided as a fallback when no "review" step is present in Checkout.
 */
class RedirectCheckoutForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {
  use StringTranslationTrait;
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new Mercado Pago object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $config_factory = $container->get('config.factory');
    return new static($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    try {
      $preference = new Preference();
    }
    catch (Exception $e) {
      // In case of comunication error,
      // notify the user and create a back button.
      watchdog('mercado_pago_sdk', 'Comuncation with Mercado Pago API failed. <br /> Message: <pre>!message</pre>',
        ['!message' => $e->getMessage()], WATCHDOG_ERROR);
      \Drupal::messenger()->addError($this->t('Comunication with the API of Mercado Pago failed. Please, contact the administrator.'));
      $form['mp_error'] = [
        '#markup' => '<div class="commerce-mercado-pago-cancel">' .
        l($this->t('Cancel payment and go back'), $url_back) . '</div>',
      ];
      return $form;
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    $order_id = $payment->getOrderId();
    $order = $payment->getOrder();

    $address = $order->getBillingProfile()->get('address')->first();

    $payer = new Payer();
    $payer->first_name = $address->getGivenName();
    $payer->last_name = $address->getFamilyName();
    if (!empty($order->getEmail())) {
      $payer->email = $order->getEmail();
    }

    $payer_address = [
      'zip_code'    => (!empty($address->getPostalCode())) ? $address->getPostalCode() : "",
      'street_name' =>
      $address->getAddressLine1() . ' / ' .
      $address->getLocality() . ' ' .
      $address->getAdministrativeArea() . ' ' .
      $address->getCountryCode() . ' / ',
    ];
    $payer->address = $payer_address;

    $payer->entity_type = "individual";
    $payer->type = "customer";

    $items = [];

    foreach ($order->getItems() as $order_item) {
      $item = new Item();
      $item->title = $order_item->getTitle();
      $item->quantity = (int) $order_item->getQuantity();
      $item->unit_price = $order_item->getUnitPrice()->getNumber();
      $item->currency_id = $order_item->getUnitPrice()->getCurrencyCode();
      $items[] = $item;
    }

    // Setting preference properties.
    $preference->items = $items;
    $preference->payer = $payer;

    $notification_url = Url::fromRoute('commerce_payment.notify', [
      'commerce_payment_gateway' => $payment->getPaymentGateway()->id(),
    ], ['absolute' => TRUE]);

    // Only IPN notification for now.
    $preference->notification_url = $notification_url->toString() . '?source_news=ipn';

    // Save External Reference.
    $preference->external_reference = $order_id;

    // Setting binary_mode so payment can only be approved or declined.
    // Pending and in-processing payments will automatically be rejected.
    $preference->binary_mode = TRUE;

    // Pending not available on binary mode.
    $url_success = $form['#return_url'];
    $url_pending = $form['#cancel_url'];
    $url_failure = $form['#cancel_url'];

    $preference->back_urls = [
      "success" => $url_success,
      "pending" => $url_pending,
      "failure" => $url_failure,
    ];

    // Auto return doesn't work on iframe mode.
    if ($configuration['redirect_mode'] != 'iframe') {
      $preference->auto_return = "approved";
      // $preference->auto_return = "all";
    }

    // Only 1 installment on this version.
    $preference->payment_methods = [
      "excluded_payment_methods" => [
        // @todo set values from configuration
        // array("id" => "master")
      ],
      "excluded_payment_types" => [
        ["id" => "ticket"],
      ],
      "installments" => 1,
    ];

    $currency_code = $order->getTotalPrice()->getCurrencyCode();

    // Initialize Shipping and Tax prices.
    $shipping_amount = new Price('0', $currency_code);
    $tax_amount = new Price('0', $currency_code);

    // Collect the adjustments.
    $adjustments = [];
    foreach ($order->collectAdjustments() as $adjustment) {
      // Skip included adjustments.
      if ($adjustment->isIncluded()) {
        continue;
      }
      // Tax & Shipping adjustments need to be handled separately.
      if ($adjustment->getType() == 'shipping') {
        $shipping_amount = $shipping_amount->add($adjustment->getAmount());
      }
      // Add taxes that are not included in the items total.
      elseif ($adjustment->getType() == 'tax') {
        $tax_amount = $tax_amount->add($adjustment->getAmount());
      }
      else {
        // Collect other adjustments.
        $type = $adjustment->getType();
        $source_id = $adjustment->getSourceId();
        if (empty($source_id)) {
          // Adjustments without a source ID are always shown standalone.
          $key = count($adjustments);
        }
        else {
          // Adjustments with the same type and source ID are combined.
          $key = $type . '_' . $source_id;
        }

        if (empty($adjustments[$key])) {
          $adjustments[$key] = [
            'type' => $type,
            'label' => (string) $adjustment->getLabel(),
            'total' => $adjustment->getAmount(),
          ];
        }
        else {
          $adjustments[$key]['total'] = $adjustments[$key]['total']->add($adjustment->getAmount());
        }
      }
    }

    // free_shipping??
    $shipments = new Shipments();

    $shipments->mode = "not_specified";
    $shipments->cost = (int) $shipping_amount->getNumber();

    $preference->shipments = $shipments;

    // @todo Add taxes.
    // Save and POST preference.
    $preference->save();

    $preference_id = $preference->id;

    // Sandbox init point isn't working on Mercado Pago at the moment.
    // $form['#action'] =  $preference->sandbox_init_point;.
    // Only init point work, even for test mode.
    $form['#action'] = $preference->init_point;

    $order->setData('commerce_mercado_pago_checkout', [
      'preference_id' => $preference_id,
    ]);

    $order->save();

    if ($configuration['mode'] == 'test') {
      $public_key = $configuration['public_key_test'];
    }
    else {
      $public_key = $configuration['public_key_prod'];
    }

    $data = [
      'return' => $form['#return_url'],
      'cancel' => $form['#cancel_url'],
      'preference_id' => $preference_id,
      'public_key' => $public_key,
      'redirect_url' => $preference->init_point,
      'total' => $payment->getAmount()->getNumber(),
    ];

    if ($configuration['redirect_mode'] === 'iframe') {
      $form['#attached']['library'][] = 'commerce_mercado_pago/checkout_pro_review';
      $form['#attached']['drupalSettings']['commerceMercadoPago'] = $data;

      $form['#attributes']['id'] = 'commerce-mercado-pago-form';

      $iframe = '<iframe onreturn="' . $form['#return_url'] . '" src="' . $preference->init_point . '" name="MP-Checkout" scrolling="yes" frameborder="0" width="100%" height="800px"></iframe>';

      $form['iframe'] = [
        '#markup' => Markup::create($iframe),
      ];

      return $form;

    }

    return $this->buildRedirectForm(
      $form,
      $form_state,
      $preference->init_point,
      $data,
      $configuration['redirect_mode']
    );

  }

}
