<?php

namespace Drupal\commerce_mercado_pago\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodStorageInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Checkboxes;
use GuzzleHttp\Exception\BadResponseException;
use MercadoPago\MerchantOrder;
use MercadoPago\Payment;
use MercadoPago\Refund;
use MercadoPago\SDK;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Mercado Pago Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "mercado_pago_checkout_pro",
 *   label = @Translation("Mercado Pago CheckoutPro"),
 *   display_label = @Translation("Mercado Pago"),
 *   modes = {
 *     "test" = @Translation("Sandbox"),
 *     "live" = @Translation("Live"),
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_mercado_pago\PluginForm\Checkout\RedirectCheckoutForm",
 *   },
 *   payment_method_types = {"mercado_pago_checkout_pro"},
 *   credit_card_types = {
 *      "amex", "mastercard", "visa",
 *   },
 * )
 */
class CheckoutPro extends OffsitePaymentGatewayBase implements SupportsRefundsInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;


  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    if ($this->getMode() == 'test') {
      if (!empty($this->configuration['access_token_test'])) {
        SDK::setAccessToken($this->configuration['access_token_test']);
        SDK::setPublicKey($this->configuration['public_key_test']);
      }
    }
    else {
      if (!empty($this->configuration['access_token_prod'])) {
        SDK::setAccessToken($this->configuration['access_token_prod']);
        SDK::setPublicKey($this->configuration['public_key_prod']);
      }
    }

    // SDK::setClientId($this->configuration['client_id']);
    // SDK::setClientSecret($this->configuration['client_secret']);.
  }

  /**
   * Re-initializes the SDK after the plugin is unserialized.
   */
  public function __wakeup() {

    if ($this->getMode() == 'test') {
      if (!empty($this->configuration['access_token_test'])) {
        SDK::setAccessToken($this->configuration['access_token_test']);
        SDK::setPublicKey($this->configuration['public_key_test']);
      }
    }
    else {
      if (!empty($this->configuration['access_token_prod'])) {
        SDK::setAccessToken($this->configuration['access_token_prod']);
        SDK::setPublicKey($this->configuration['public_key_prod']);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'public_key_test' => '',
      'access_token_test' => '',
      'public_key_prod' => '',
      'access_token_prod' => '',
      'client_id' => '',
      'client_secret' => '',
      'redirect_mode' => 'iframe',
      'payment_method' => [],
      'accepted_cards' => '',
      'autofee' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['public_key_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public key Test'),
      '#description' => $this->t('The Public key Test for Mercado Pago.'),
      '#default_value' => $this->configuration['public_key_test'],
    ];

    $form['access_token_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token Test'),
      '#description' => $this->t('The Access token Test for Mercado Pago.'),
      '#default_value' => $this->configuration['access_token_test'],
    ];

    $form['public_key_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public key Prod'),
      '#description' => $this->t('The Public key Prod for Mercado Pago.'),
      '#default_value' => $this->configuration['public_key_prod'],
    ];

    $form['access_token_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token Prod'),
      '#description' => $this->t('The Access token Prod for Mercado Pago.'),
      '#default_value' => $this->configuration['access_token_prod'],
    ];

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client id'),
      '#description' => $this->t('The Client id for Mercado Pago.'),
      '#default_value' => $this->configuration['client_id'],
    ];

    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#description' => $this->t('The Client Secret for Mercado Pago.'),
      '#default_value' => $this->configuration['client_secret'],
    ];

    $form['redirect_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Checkout redirect mode'),
      '#description' => $this->t('The redirect mode for this checkout.'),
      '#default_value' => $this->configuration['redirect_mode'],
      '#required' => TRUE,
      '#options' => [
        'iframe' => $this->t('Stay on this site using an iframe to embed the hosted checkout page'),
        'post' => $this->t('Redirect to the hosted checkout page via POST through an automatically submitted form'),
        'get' => $this->t('Redirect to the hosted checkout page immediately with a GET request'),
      ],
    ];

    $form['payment_method'] = [
      '#type' => 'checkboxes',
      '#id' => 'mercado-pago-method',
      '#title' => $this->t('Accepted payment methods'),
      '#description' => $this->t('Which payment methods to accept. @TODO'),
      '#default_value' => array_keys($this->configuration['payment_method']),
      // '#attributes' => array('checked' => 'checked'),
      '#options' => [
        'creditcard' => $this->t('Creditcard'),
        'mercado-pago' => $this->t('Mercado Pago Web'),
        'qr-app' => $this->t('Mercado Pago QR Code'),
      ],
    ];

    $options = [];
    // Add image to the cards where defined.
    foreach ($this->getMercadoPagoCards() as $key => $card) {
      $options[$key] = empty($card['image']) ? $card['name'] : '<img src="/' . $card['image'] . '" /> ' . $card['name'];
    }

    $form['accepted_cards'] = [
      '#type' => 'checkboxes',
      '#id' => 'mercado-pago-cards',
      '#title' => $this->t('Select accepted cards'),
      '#description' => $this->t('Which credit cards to accept. @TODO'),
      '#default_value' => $this->configuration['accepted_cards'],
      // '#attributes' => array('checked' => 'checked'),
      '#options' => $options,
      '#states' => [
        'visible' => [
          ':input[name="configuration[mercado_pago_checkout_pro][payment_method][creditcard]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['autofee'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autofee'),
      '#description' => $this->t('If set, the fee charged by the acquirer will be calculated and added to the transaction amount. @TODO'),
      '#default_value' => $this->configuration['autofee'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    if ($values['mode'] == 'test') {
      if (empty($values['access_token_test'])) {
        $form_state->setError($form['access_token_test'], $this->t('Access token test cannot be empty.'));
      }
      if (empty($values['public_key_test'])) {
        $form_state->setError($form['public_key_test'], $this->t('Public key test cannot be empty.'));
      }

    }
    else {
      if (empty($values['access_token_prod'])) {
        $form_state->setError($form['access_token_prod'], $this->t('Access token prod cannot be empty.'));
      }
      if (empty($values['public_key_prod'])) {
        $form_state->setError($form['public_key_prod'], $this->t('Public key prod cannot be empty.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['public_key_test'] = $values['public_key_test'];
      $this->configuration['access_token_test'] = $values['access_token_test'];
      $this->configuration['public_key_prod'] = $values['public_key_prod'];
      $this->configuration['access_token_prod'] = $values['access_token_prod'];
      $this->configuration['client_id'] = $values['client_id'];
      $this->configuration['client_secret'] = $values['client_secret'];
      $this->configuration['redirect_mode'] = $values['redirect_mode'];

      $payment_methods = Checkboxes::getCheckedCheckboxes($values['payment_method']);
      $enabled_payment_methods = [];
      foreach ($payment_methods as $payment_method) {
        $enabled_payment_methods[$payment_method] = $payment_method;
      }
      $this->configuration['payment_method'] = $enabled_payment_methods;

      $accepted_cards = Checkboxes::getCheckedCheckboxes($values['accepted_cards']);
      $enabled_accepted_cards = [];
      foreach ($accepted_cards as $accepted_card) {
        $enabled_accepted_cards[$accepted_card] = $accepted_card;
      }
      $this->configuration['accepted_cards'] = $enabled_accepted_cards;
      $this->configuration['autofee'] = $values['autofee'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    // $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    $logger = \Drupal::logger('Mercado Pago Refund');

    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    // $this->assertRefundAmount($payment, $amount);
    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);

    try {
      if ($this->getMode() == 'test') {
        SDK::setAccessToken($this->configuration['access_token_test']);
      }
      else {
        SDK::setAccessToken($this->configuration['access_token_prod']);
      }
      $refund = new Refund();
      $refund->payment_id = $payment->getRemoteId();
      $refund->amount = floatval($amount->getNumber());
      $response = $refund->save();
    }
    catch (BadResponseException $exception) {
      $logger->error($exception->getResponse()->getBody()->getContents());
      throw new PaymentGatewayException('An error occurred while refunding the payment.');
    }

    if (!empty($refund->error)) {
      $logger->debug('Refund error: <pre>@data</pre>', [
        '@data' => print_r($refund, TRUE),
      ]);
      throw new PaymentGatewayException("Refund error", $refund->error);
    }

    if ($new_refunded_amount < $payment->getAmount()) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }
    $payment->setRefundedAmount($new_refunded_amount);
    $payment->setRemoteState($response->status);
    $payment->save();

    $order_id = $payment->getOrder()->id();
    $logger->info('Refund completed.<br>Refunded amount: ' . $amount . '<br>Order id: ' . $order_id . '<br>Payment: ' . $payment->getRemoteId());

  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {

    $logger = \Drupal::logger('Mercado Pago Notifications');

    /*
    $logger->debug('Headers: <pre>@data</pre>', [
    '@data' => print_r($request->headers, TRUE),
    ]);
     */

    // Reacts to IPN events.
    $response = $request->query->all();
    // $logger->info('Response: ' . print_r($response, TRUE));
    if ($this->getMode() == 'test') {
      SDK::setAccessToken($this->configuration['access_token_test']);
    }
    else {
      SDK::setAccessToken($this->configuration['access_token_prod']);
    }

    if (isset($response["topic"])) {
      // https://www.mercadopago.com.uy/developers/es/docs/checkout-pro/additional-content/your-integrations/notifications/ipn
      $merchant_order = NULL;
      switch ($response["topic"]) {
        case "payment":
          // Get the payment and the corresponding
          // merchant_order reported by the IPN.
          $ipn_payment = Payment::find_by_id($response["id"]);
          $merchant_order = MerchantOrder::find_by_id($ipn_payment->order->id);
          break;

        case "merchant_order":
          $merchant_order = MerchantOrder::find_by_id($response["id"]);
          break;
      }

      $order_id = $merchant_order->external_reference;
      $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);

      if ($response["topic"] == "merchant_order") {

        $logger->debug('Merchant order data: <pre>@data</pre>', [
          '@data' => print_r($merchant_order, TRUE),
        ]);

        $paid_amount = 0;
        foreach ($merchant_order->payments as $mo_payment) {
          if ($mo_payment->status == 'approved') {
            $paid_amount += $mo_payment->total_paid_amount;
          }
        }

        // If the payment's transaction amount is equal (or bigger)
        // than the merchant_order's amount you can release your items.
        if ($paid_amount >= $merchant_order->total_amount) {
          if (!is_null($merchant_order->shipments) && $merchant_order->shipping_cost > 0) {
            // If the merchant_order has shipments
            // if($merchant_order->shipments[0]->status == "ready_to_ship") {.
            $logger->info("Totally paid. Print the label and release your item.<br>Order ID: " . $order_id . "<br>Paid amount: " . $paid_amount);
            // }
          }
          else {
            // The merchant_order doesn't have any shipments.
            $logger->info("Totally paid. Release your item.<br>Order ID: " . $order_id . "<br>Paid amount: " . $paid_amount);
          }
        }
        else {
          $logger->info("Order received at Mercado Pago.<br>Order ID: " . $order_id . "<br>Paid amount: " . $paid_amount);
        }

      }
      else {

        // $response["topic"] == "payment"
        if (isset($ipn_payment)) {

          $logger->debug('Payment data: <pre>@data</pre>', [
            '@data' => print_r($ipn_payment, TRUE),
          ]);

          $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

          $payment_ids = $payment_storage->getQuery()
            ->condition('order_id', $order_id)
            ->condition('remote_id', $ipn_payment->id)
            ->accessCheck(FALSE)
            ->sort('payment_id', 'DESC')
            ->range(0, 1)
            ->execute();

          // If payment was already saved we don't need to save it again.
          if (!$payment_ids) {

            $payment_method = NULL;

            if ($order instanceof OrderInterface && !$order->get('payment_method')->isEmpty()) {
              /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
              $payment_method = $order->get('payment_method')->entity;
            }
            // If the order doesn't reference a payment method,
            // or if the payment method doesn't reference
            // the right gateway, create a new one.
            if (!$payment_method || $payment_method->getPaymentGatewayId() !== $this->parentEntity->id()) {
              // Create a payment method.
              $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
              $payment_method = $payment_method_storage->createForCustomer(
                'mercado_pago_checkout_pro',
                $this->parentEntity->id(),
                $order->getCustomerId(),
                $order->getBillingProfile()
              );
            }

            $payment_method->setRemoteId($merchant_order->id);
            $payment_method->setReusable(FALSE);
            $payment_method->save();

            $order->set('payment_method', $payment_method);
            $order->save();

            $total_amount = Price::fromArray([
              'number' => $ipn_payment->transaction_details->total_paid_amount,
              'currency_code' => $ipn_payment->currency_id,
            ]);

            $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

            $payment_order = $payment_storage->create([
              'state' => 'authorization',
              'amount' => $total_amount,
              'payment_gateway' => $this->parentEntity->id(),
              'order_id' => $order->id(),
              'remote_id' => $ipn_payment->id,
              'remote_state' => $ipn_payment->status,
            ]);
            $payment_order->save();

            $payment_order->setState('completed');
            $payment_order->save();

            $logger->info('Payment information saved successfully.<br>Order id: ' . $order->id() . '<br>Payment Type: ' . $ipn_payment->payment_type_id . '<br>Amount: ' . $total_amount);

          }
          else {

            // If it is a refund we update the payment.
            if ($ipn_payment->status == 'refunded') {
              $total_amount = Price::fromArray([
                'number' => $ipn_payment->transaction_amount_refunded,
                'currency_code' => $ipn_payment->currency_id,
              ]);

              $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
              $payment_order = $payment_storage->loadByRemoteId($ipn_payment->id);

              if ($ipn_payment->transaction_details->total_paid_amount > $ipn_payment->transaction_amount_refunded) {
                $payment_order->setState('partially_refunded');
              }
              else {
                $payment_order->setState('refunded');
              }

              $payment_order->setRemoteState($ipn_payment->status);
              $payment_order->setRefundedAmount($total_amount);
              $payment_order->save();

              $logger->info("Refund.<br>Order ID: " . $order->id() . "<br>Amount refunded: " . $total_amount);

            }
          }
        }
      }
    }

    // Return empty response with 200 status code.
    return new JsonResponse();

  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {

    $logger = \Drupal::logger('Mercado Pago OnReturn');

    $order_checkout_pro_data = $order->getData('commerce_mercado_pago_checkout');
    if (empty($order_checkout_pro_data['preference_id'])) {
      throw new PaymentGatewayException('Preference id not found for this Checkout transaction.');
    }

    $response = $request->query->all();

    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    // If payment was already saved with IPN method (OnNotify)
    // no need to save the payment again.
    $payment_ids = $payment_storage->getQuery()
      ->condition('order_id', $order->id)
      ->condition('payment_id', $response['payment_id'])
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();

    if (count($payment_ids) > 0) {
      $logger->info("Payment already saved with IPN notifications");
      return;
    }
    // If OnNotify method failed for some reason
    // we continue to save the payment information.
    $order->setData('commerce_mercado_pago_checkout', [
      'remote_id' => $response['merchant_order_id'],
    ]);

    /*
    // If the user is anonymous, add their Mercado Pago e-mail to the order.
    if (empty($order->getEmail())) {
    //@todo get user email from Mercado Pago
    $user_email = $response['email']
    $order->setEmail($user_email);
    }
     */

    if ($response['status'] !== 'approved') {
      $logger->error('An error occurred on the payment process: <pre>@response</pre>', [
        '@response' => print_r($response, TRUE),
      ]);
      throw new PaymentGatewayException("An error occurred on the payment process", $response['status']);
    }

    $payment_method = NULL;
    // If a payment method is already referenced by the order, no need to create
    // a new one.
    if (!$order->get('payment_method')->isEmpty()) {
      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = $order->get('payment_method')->entity;
    }
    // If the order doesn't reference a payment method yet, or if the payment
    // method doesn't reference the right gateway, create a new one.
    if (!$payment_method || $payment_method->getPaymentGatewayId() !== $this->parentEntity->id()) {
      // Create a payment method.
      $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
      assert($payment_method_storage instanceof PaymentMethodStorageInterface);
      $payment_method = $payment_method_storage->createForCustomer(
        'mercado_pago_checkout_pro',
        $this->parentEntity->id(),
        $order->getCustomerId(),
        $order->getBillingProfile()
      );
    }

    $payment_method->setRemoteId($response['merchant_order_id']);
    $payment_method->setReusable(FALSE);
    $payment_method->save();

    $order->set('payment_method', $payment_method);
    $order->save();

    $logger->info('Payment information saved successfully.<br>Payment Type: ' . $response['payment_type'] . '<br>Merchant order id: ' . $response['merchant_order_id'] . '<br>Preference id: ' . $response['preference_id']);

    $payment = $payment_storage->create([
      'state' => 'authorization',
      'amount' => $order->getBalance(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $response['payment_id'],
      'remote_state' => $response['status'],
    ]);
    $payment->save();

    $payment->setState('completed');
    $payment->save();

    $this->messenger()->addStatus('Payment has been successfully processed.');

  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {

    // @todo createPaymentMethod
    $card_types = CreditCard::getTypes();
    if (!isset($card_types[$payment_source['brand']])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $request['payment_type']));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * Builds a customer profile, assigned to the order's owner.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\profile\Entity\ProfileInterface
   *   The customer profile.
   */
  protected function buildCustomerProfile(OrderInterface $order) {
    return $this->entityTypeManager->getStorage('profile')->create([
      'uid' => $order->getCustomerId(),
      'type' => 'customer',
    ]);
  }

  /**
   * Information about all supported cards.
   *
   * @return array
   *   Array with card name and image.
   */
  protected function getMercadoPagoCards() {
    $module_path = \Drupal::service('extension.path.resolver')->getPath('module', 'commerce_mercado_pago');
    $path_to_images = $module_path . "/images/";

    if (file_exists($path_to_images . 'amexpress.jpg') &&
        file_exists($path_to_images . 'mastercard.jpg') &&
        file_exists($path_to_images . 'visa.jpg')) {

      return [
        'amex' => [
          'name' => $this->t('American Express'),
          'image' => $path_to_images . 'amexpress.jpg',
        ],
        'mastercard' => [
          'name' => $this->t('Mastercard'),
          'image' => $path_to_images . 'mastercard.jpg',
        ],
        'visa' => [
          'name' => $this->t('Visa'),
          'image' => $path_to_images . 'visa.jpg',
        ],
      ];
    }
    else {
      return [
        'amex' => [
          'name' => $this->t('American Express'),
        ],
        'mastercard' => [
          'name' => $this->t('Mastercard'),
        ],
        'visa' => [
          'name' => $this->t('Visa'),
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));
  }

}
